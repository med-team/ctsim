#!/bin/sh

DIR=/tmp
rm -rf $DIR/sng $DIR/omp
mkdir $DIR/sng $DIR/omp

(cd ..; make clean; (./configure --disable-openmp > /dev/null); grep HAVE_OPEN config.h; (make -j 20 > /dev/null); cp tools/ctsimtext src/ctsim $DIR/sng/.)
(cd ..; make clean; (./configure --enable-openmp > /dev/null); grep HAVE_OPEN config.h; (make -j 20 > /dev/null); cp tools/ctsimtext src/ctsim $DIR/omp/.)
